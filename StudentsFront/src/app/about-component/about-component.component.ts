import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-about-component',
  templateUrl: './about-component.component.html',
  styleUrls: ['./about-component.component.css']
})
export class AboutComponentComponent implements OnInit {

  constructor() { }
  date: Date;

  ngOnInit(): void {
    this.getDate();
  }
  getDate(): void{
    this.date = new Date();
  }

}
